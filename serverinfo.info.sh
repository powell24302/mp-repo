#! /bin/bash

#Server info

#Show the date 
echo "Date: $(date)" >> /tmp/serverinfo.info

#Show 10 last user
echo "last 10 users"
last -n 10 >> /tmp/serverinfo.info

#Show swap space info
echo "swap space"
free -h  >> /tmp/serverinfo.info

#Show the kernal version
echo "kernel version"
uname -r >> /tmp/serverinfo.info

#Show ip address
echo "ip address"
ip addr show >> /tmp/serverinfo.info
